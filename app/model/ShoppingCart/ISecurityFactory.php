<?php

namespace App\Model\ShoppingCart;

interface ISecurityFactory
{
    /** @return Security */
    function create();
}