<?php

namespace App\Model\ShoppingCart;

class Security {

    const
            PRIVATE_KEY = '5K4i95gvphVHJocfRa8d',
            HASH = 'sha256',
            TOKEN_EXPIRATION = 3000; //50 minutes
    
    /**
     * Request time
     * @var int
     */
    private $timestamp;
    
    /**
     * Security token
     * @var string
     */
    private $token;
    
    /**
     * @var string
     */
    private $key = '';
    
    /**     
     * @param string $key
     */
    public function setKey($key) {
        $this->key = $key;
        return $this;
    }
    
    /**     
     * @param string $token
     */
    public function setToken($token) {
        $this->token = $token;
        return $this;
    }
    
    /**     
     * @param int $timestamp
     */
    public function setTimestamp($timestamp) {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * Create security token
     * @param int $event_id
     * @param int $order_id
     * @return array
     */
    public function make() {
        $date = new \DateTime;
        $this->timestamp = $date->getTimestamp();
        $this->token = hash_hmac(self::HASH, $this->key . $this->timestamp, self::PRIVATE_KEY);
        return array('timestamp' => $this->timestamp, 'token' => $this->token);
    }

    /**
     * Verifies the security token
     * @throws \ErrorException
     */
    public function verifi() {
        $date = new \DateTime;
        $diff = $this->timestamp - self::TOKEN_EXPIRATION;
        if ($diff > $date->getTimestamp()) {
            throw new \ErrorException('Request timeout. Please make your order again.');
        }

        $interToken = hash_hmac(self::HASH, $this->key . $this->timestamp, self::PRIVATE_KEY);
        if ($this->token !== $interToken) {
            throw new \ErrorException('Token does not match. Possible XSS attack. Please make your order again.');
        }
        return TRUE;
    }

}
