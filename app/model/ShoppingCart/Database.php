<?php

namespace App\Model\ShoppingCart;

use \Nette\Database\Context;

class Database {

    /**
     * @var \Nette\Database\Context
     */
    private $databse;

    /**
     * @param \Nette\Database\Context $database
     */
    public function __construct(Context $database) {
        $this->databse = $database;
    }

    /**
     * @param int $id
     * @return \Nette\database\Table\ActiveRow
     */
    public function findItemattributecombinationById($id) {
        return $this->findItemattributecombination()->get($id);
    }

    /**
     * Create new cart
     * @return \Nette\database\Table\ActiveRow
     */
    public function createCart() {
        return $this->findCart()->insert(array('dateAdd' => new \DateTime));
    }

    /**
     * Load item from cart
     * @param int $cart_id
     * @return Nette\Database\Table\Selection
     */
    public function loadCart($cart_id) {
        return $this->loadCartById($cart_id)->related('cartitem', 'cart_id')->where('delete IS NULL');
    }
    
    /**
     * Load item from cart
     * @param int $cart_id
     * @return Nette\Database\Table\Selection
     */
    public function loadCartById($cart_id) {
        return $this->findCart()->get($cart_id);
    }

    /**
     * Save item to the cart
     * @param int $cart_id
     * @param \App\Model\ShoppingCart\Item $item
     */
    public function saveItemToCart($cart_id, Item $item) {
        $this->findCartitem()->insert(array(
            'cart_id' => $cart_id,
            'item_id' => $item->getId(),
            'itemattributecombination_id' => $item->getAttributeId(),
            'price' => $item->getPrice(),
            'quantity' => $item->getQuantity(),
            'dateAdd' => new \DateTime
        ));
    }

    /**
     * Update item quantity
     *  in the cartt
     * @param int $cart_id
     * @param \App\Model\ShoppingCart\Item $item
     */
    public function updateItemCartQuantity($cart_id, Item $item) {
        $this->findCartitem()->where('cart_id', $cart_id)->where('item_id', $item->getId())->where('itemattributecombination_id', $item->getAttributeId())->update(array(
            'quantity' => $item->getQuantity()
        ));
    }

    /**
     * Delete item from the cart
     * @param int $cart_id
     * @param \App\Model\ShoppingCart\Item $item
     */
    public function deleteItemFromCart($cart_id, Item $item) {
        $this->findCartitem()->where('cart_id', $cart_id)->where('item_id', $item->getId())->where('itemattributecombination_id', $item->getAttributeId())->update(array(
            'delete' => new \DateTime
        ));
    }

    /**
     * Return items placed in cart
     * @return \Nette\Database\Table\Selection
     */
    public function getItesmInCart() {
        return $this->findCartitem()->where('delete IS NULL')->where('cart.dateAdd > DATE_ADD(NOW(),INTERVAL -2 DAY)')->where('cart.dateFinished IS NULL');
    }

    /**
     * Insert address to database
     * @param array $data
     * @param int $cart_id
     */
    public function saveAddress($data, $cart_id) {
        $this->findCartAddress()->where('cart_id', $cart_id)->delete();
        $row = $this->findAddress()->insert(array(
            'email' => $data->email,
            'company' => $data->company,
            'firstname' => $data->firstname,
            'surname' => $data->surname,
            'address' => $data->address,
            'city' => $data->city,
            'postcode' => $data->postcode,
            'country' => $data->country,
            'phone' => $data->phone,
            'dateAdd' => new \DateTime
        ));
        $this->findCartAddress()->insert(array(
            'cart_id' => $cart_id,
            'address_id' => $row->id,
            'addresstype_id' => 1
        ));

        if ($data->secondAddress) {
            $this->findCartAddress()->insert(array(
                'cart_id' => $cart_id,
                'address_id' => $row->id,
                'addresstype_id' => 2
            ));
        } else {
            $row = $this->findAddress()->insert(array(
                'email' => $data->email2,
                'company' => $data->company2,
                'firstname' => $data->firstname2,
                'surname' => $data->surname2,
                'address' => $data->address2,
                'city' => $data->city2,
                'postcode' => $data->postcode2,
                'country' => $data->country2,
                'phone' => $data->phone2,
                'dateAdd' => new \DateTime
            ));
            $this->findCartAddress()->insert(array(
                'cart_id' => $cart_id,
                'address_id' => $row->id,
                'addresstype_id' => 2
            ));
        }
    }
    
    /**
     * Insert discount coupon
     * @param array $data
     * @param int $cart_id
     */
    public function useDiscount($data, $cart_id) {
        $row = $this->findDiscount()->where('code', $data->discount)->fetch();
        if ($row) {
            $this->findCart()->where('id', $cart_id)->update(array(
                'discount_id' => $row->id
            ));
        }
    }
    
    /**
     * Return cart addresess
     * @param int $cart_id
     * @return \Nette\Database\Table\Selection
     */
    public function getCartAddresses($cart_id) {
        return $this->findCartAddress()->where('cart_id', $cart_id);
    }
    
    /**
     * Insert data to order table
     * @param int $cart_id
     * @param float $price
     * @return \Nette\database\Table\ActiveRow
     */
    public function confirmOrder($cart_id, $price) {
        $this->loadCartById($cart_id)->update(array('dateFinished' => new \DateTime));
        return $this->findOrder()->insert(array(
            'cart_id' => $cart_id,
            'price' => $price,
            'date' => new \DateTime
        ));
    }
    
    /**
     * Update stock quantity
     * @param Item $item
     */
    public function updateQuantity($item) {
        $this->findItemattributecombination()->where('id', $item->getAttributeId())->update(array(
            'quantity' => new \Nette\Database\SqlLiteral('quantity - ' . $item->getQuantity())
        ));
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    private function findItemattributecombination() {
        return $this->databse->table('itemattributecombination');
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    private function findCart() {
        return $this->databse->table('cart');
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    private function findCartitem() {
        return $this->databse->table('cartitem');
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function findItem() {
        return $this->databse->table('item');
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    private function findAddress() {
        return $this->databse->table('address');
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    private function findCartAddress() {
        return $this->databse->table('cartaddress');
    }
    
    /**
     * @return \Nette\Database\Table\Selection
     */
    private function findDiscount() {
        return $this->databse->table('discount');
    }
    
    /**
     * @return \Nette\Database\Table\Selection
     */
    private function findOrder() {
        return $this->databse->table('order');
    }
    
    

}
