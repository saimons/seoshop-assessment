<?php

namespace App\Model\ShoppingCart;

class Stock {

    /**
     * @var \App\Model\ShoppingCart\Database
     */
    private $database;

    /**
     * Array of items 
     * @var array array of Item
     */
    private $items = array();

    public function __construct(\App\Model\ShoppingCart\Database $database) {
        $this->database = $database;
        $this->intItems();
    }

    /**
     * Prepare quantity of items
     */
    private function intItems() {
        $items = $this->database->findItem();
        $itemsInCart = $this->database->getItesmInCart();

        $usedItem = array();
        foreach ($itemsInCart AS $itemDB) {
            $item = new Item($itemDB->itemattributecombination);
            $key = $item->createProductKey();
            if (!isset($usedItem[$key])) {
                $usedItem[$key] = 0;
            }
            $usedItem[$key] += $itemDB->quantity;
        }

        foreach ($items AS $itemDB) {
            foreach ($itemDB->related('itemattributecombination', 'item_id') AS $variant) {
                $item = new Item($variant);
                $key = $item->createProductKey();
                $this->items[$key] = $item;
                $quantity = isset($usedItem[$key]) ? $variant->quantity - $usedItem[$key] : $variant->quantity;
                $this->items[$key]->addQuantity($quantity);
            }
        }
    }
    
    /**
     * Return all items with stock
     * @return array Array of Item
     */
    public function getItems() {
        return $this->items;
    }

}
