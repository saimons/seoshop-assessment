<?php

namespace App\Model\ShoppingCart;

class Item {

    /**
     * @var int
     */
    private $id;

    /**
     * Product name
     * @var string
     */
    private $name;

    /**     
     * @var int
     */
    private $attributeId;
    
    /**
     * Attribute name 
     * @var string
     */
    private $attributeName;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int
     */
    private $inStock;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var \Nette\Database\Table\selection 
     */
    private $itemattributecombination;
        

    /**
     * 
     * @param \Nette\Database\Table\selection $itemattributecombination
     */
    public function __construct($itemattributecombination) {
        $this->itemattributecombination = $itemattributecombination;
        $this->parseAttributes();
    }
    
    /**
     * Insert quantity of product
     * @param type $quantity
     */
    public function insertQuantity($quantity) {
        $this->quantity = $quantity;
    }

    /**
     * Increase quantity of product
     * @param type $quantity
     */
    public function addQuantity($quantity) {
        $this->quantity += $quantity;
    }
    
    /**     
     * @return int
     */
    public function getId() {
        return $this->id;
    }
    
    /**     
     * @return int
     */
    public function getAttributeId() {
        return $this->attributeId;
    }
    
    /**     
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }
    
    /**     
     * @return string
     */
    public function getName() {
        return $this->name;
    }
    
    /**     
     * @return string
     */
    public function getAttributeName() {
        return $this->attributeName;
    }

    /**     
     * @return int
     */
    public function getQuantity() {
        return $this->quantity;
    }
    
    /**     
     * @return float
     */
    public function getTotalPrice() {
        return $this->quantity * $this->price;
    }

    /**
     * Create unique key for product and his combination
     * @return string
     */
    public function createProductKey() {
        return md5($this->id . $this->name . $this->price);
    }

    /**
     * Parse attributes from database
     */
    private function parseAttributes() {
        $this->id = $this->itemattributecombination->item->id;
        $this->name = $this->itemattributecombination->item->name;
        $this->attributeId = $this->itemattributecombination->id;
        $this->attributeName = $this->itemattributecombination->name;
        $this->price = $this->itemattributecombination->price;
        $this->inStock = $this->itemattributecombination->quantity;
    }

}
