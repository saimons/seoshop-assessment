<?php

namespace App\Model\ShoppingCart;

class Cart {

    /**
     * @var \App\Model\ShoppingCart\Database
     */
    private $database;

    /**
     * @var Nette\Http\SessionSection 
     */
    private $sessionCart;

    /**
     * @var \App\Model\ShoppingCart\Stock
     */
    public $stock;

    /**
     * @var int
     */
    private $cart_id;

    /**
     * Array of items in cart 
     * @var array array of Item
     */
    private $items = array();
    
    /**
     * Percentage of discount
     * @var float
     */
    private $discount;

    public function __construct(\App\Model\ShoppingCart\Database $database, \Nette\Http\Session $session, \App\Model\ShoppingCart\Stock $stock) {
        $this->database = $database;
        $this->sessionCart = $session->getSection('cart');
        $this->stock = $stock;
        $this->cartInit();
    }

    /**
     * Inicialization of cart according to session id if is not exist create new one
     */
    private function cartInit() {
        if (isset($this->sessionCart->cart_id)) {
            $this->cart_id = $this->sessionCart->cart_id;
            $this->loadDataFromDB();
        } else {
            $this->sessionCart->setExpiration('+ 2 days');
            $this->cart_id = $this->database->createCart()->id;
            $this->sessionCart->cart_id = $this->cart_id;
        }
    }
    
    /**     
     * @return int
     */
    public function getCart_id() {
        return $this->cart_id;
    }

    /**
     * Add product to the cart
     * @param string $key MD5 hash of item combination
     * @param int $quantity
     * @throws InvalidArgumentException
     */
    public function addItem($key, $quantity = 1) {
        $items = $this->stock->getItems();

        if (!isset($items[$key])) {
            throw new \InvalidArgumentException('Item with this ID is not exist.');
        }

        if ($items[$key]->getQuantity() <= 0) {
            throw new \InvalidArgumentException('There is not enough goods available.');
        }

        if (!isset($this->items[$key])) {
            $this->items[$key] = $items[$key];
            $this->items[$key]->insertQuantity($quantity);
            $this->database->saveItemToCart($this->cart_id, $this->items[$key]);
        } else {
            $this->updateItem($key, $quantity);
        }
    }

    /**
     * Return product in the cart
     * @return array
     */
    public function getItems() {
        return count($this->items) ? $this->items : NULL;
    }
    
    /**
     * Amount of discount
     * @return float
     */
    public function getDiscount() {
        return $this->discount;
    }

    /**
     * Delete item from the cart
     * @param string $key MD5 hash
     */
    public function deleteItem($key) {
        $this->database->deleteItemFromCart($this->cart_id, $this->items[$key]);
        unset($this->items[$key]);
    }

    /**
     * 
     * @param string $key MD5 hash of item combination
     * @param int $quantity
     */
    public function updateItem($key, $quantity = 1) {
        $items = $this->stock->getItems();
        
        if ($items[$key]->getQuantity() > 0 OR $quantity < 0) {
            $this->items[$key]->addQuantity($quantity);
        }

        if ($this->items[$key]->getQuantity() <= 0) {
            $this->items[$key]->insertQuantity(0);
        }

        $this->database->updateItemCartQuantity($this->cart_id, $this->items[$key]);
        
    }

    /**
     * Return total price of product in cart
     * @return float
     */
    public function getTotalPrice() {
        $price = 0;
        foreach ($this->items AS $item) {
            $price += $item->getTotalPrice();
        }
        return $price;
    }
    
    /**
     * Return total price with discount
     * @return float
     */
    public function getTotalPriceWithDiscount() {
        return $this->getTotalPrice() * (1 - $this->discount/100);
    }

    /**
     * Return total quantity of product in cart
     * @return float
     */
    public function getTotalQuantity() {
        $quantity = 0;
        foreach ($this->items AS $item) {
            $quantity += $item->getQuantity();
        }
        return $quantity;
    }

    /**
     * Load data from DB storage
     */
    private function loadDataFromDB() {
        $cartDB = $this->database->loadCart($this->cart_id);
        $this->discount = $this->database->loadCartById($this->cart_id)->discount_id ? $this->database->loadCartById($this->cart_id)->discount->value : 0;
        
        foreach ($cartDB AS $itemDB) {
            $item = new Item($itemDB->itemattributecombination);
            $key = $item->createProductKey();
            $this->items[$key] = $item;
            $this->items[$key]->addQuantity($itemDB->quantity);
        }
    }
    
    /**
     * Confirm order and destroy cart sessions
     */
    public function confirm() {
        $order = $this->database->confirmOrder($this->cart_id, $this->getTotalPriceWithDiscount());
        foreach($this->items AS $item) {
            $this->database->updateQuantity($item);
        }
        $this->sessionCart->cart_id = NULL;
        return $order;
    }

}
