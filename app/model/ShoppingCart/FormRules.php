<?php

namespace App\Model\ShoppingCart;

use Nette\Forms\IControl;

class FormRules {

    
    const GLOBALCHARS = '\App\Model\ShoppingCart\FormRules::validateGlobalChars';
    const PHONE = '\App\Model\ShoppingCart\FormRules::validatePhone';

    
    /**
     * Validate chars for global inputs
     * @param \Nette\Forms\IControl $control
     * @return bool
     */
    public static function validateGlobalChars(IControl $control) {
        return self::globalChars($control->value);
    }
    
    /**
     * Validate phone number
     * @param \Nette\Forms\IControl $control
     * @return bool
     */
    public static function validatePhone(IControl $control) {
        return self::phone($control->value);
    }
    
    /**
     * Regular ex. for validation of global inputs
     * @param string $value
     * @return bool
     */
    private static function globalChars($value) {
        return !preg_match('/[^\p{L}\p{Nd}\p{Zs}\-\.\,\%\–\?\(\)\ ]/u', $value);
    }
    
    /**
     * Rer. ex. for phone number
     * @param string $value
     * @return bool
     */
    private static function phone($value) {
        return !preg_match('/[^\p{Nd}\-\.\+\(\)\ ]/u', $value);
    }

}
