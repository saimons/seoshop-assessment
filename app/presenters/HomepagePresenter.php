<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Model\ShoppingCart\FormRules;

class HomepagePresenter extends Nette\Application\UI\Presenter {

    /**
     * @var \App\Model\ShoppingCart\Database
     * @inject
     */
    public $database;

    /**
     * @var \App\Model\ShoppingCart\Cart
     * @inject
     */
    public $cart;

    /**
     * @var \App\Model\ShoppingCart\Stock
     * @inject
     */
    public $stock;

    /**
     * @var \App\Model\ShoppingCart\ISecurityFactory 
     * @inject
     */
    public $security;

    public function beforeRender() {
        parent::beforeRender();
        $this->template->security = $this->security;
    }

    /**
     * Main default render
     */
    public function renderDefault() {
        $this->template->cart = $this->cart;
        $this->template->items = $this->stock->getItems();
    }

    /**
     * Add item to the cart
     * @param string $key MD5 hash
     */
    public function actionInsertItemToCart($key) {
        $this->cart->addItem($key);
        $this->redirect('default');
    }

    /**
     * Delete item from the cart
     * @param string $key MD5 hash
     */
    public function actionDelete($key, $timestamp, $token) {
        try {
            $this->security->create()->setKey($key)->setTimestamp($timestamp)->setToken($token)->verifi();
        } catch (\ErrorException $e) {
            //could be sended a massage
            $this->redirect('default');
        }
        $this->cart->deleteItem($key);
        $this->redirect('default');
    }

    /**
     * Increase number of product in the cart
     * @param string $key MD5 hash
     */
    public function actionIncrease($key, $timestamp, $token) {
        try {
            $this->security->create()->setKey($key)->setTimestamp($timestamp)->setToken($token)->verifi();
        } catch (\ErrorException $e) {
            //could be sended a massage
            $this->redirect('default');
        }
        $this->cart->updateItem($key, 1);
        $this->redirect('default');
    }

    /**
     * Decrease number of product in the cart
     * @param string $key MD5 hash
     */
    public function actionDecrease($key, $timestamp, $token) {
        try {
            $this->security->create()->setKey($key)->setTimestamp($timestamp)->setToken($token)->verifi();
        } catch (\ErrorException $e) {
            //could be sended a massage
            $this->redirect('default');
        }
        $this->cart->updateItem($key, -1);
        $this->redirect('default');
    }

    /**
     * Filing address
     * @param int $timestamp
     * @param string $token
     */
    public function renderAddress($timestamp, $token) {
        try {
            $this->security->create()->setTimestamp($timestamp)->setToken($token)->verifi();
        } catch (\ErrorException $e) {
            //could be sended a massage
            $this->redirect('default');
        }
    }

    protected function createComponentRegistrationForm() {
        $form = new Form;
        $lenght = 60;

        $form->addText('company', 'Company', $lenght, 100)
                ->addCondition(Form::FILLED)
                ->addRule(FormRules::GLOBALCHARS, 'In your "Company name" are used invalid characters.');

        $form->addText('firstname', 'First name', $lenght, 100)
                ->setRequired('Enter your "First name".')
                ->addRule(FormRules::GLOBALCHARS, 'In your "First name" are used invalid characters.');

        $form->addText('surname', 'Second name', $lenght, 100)
                ->setRequired('Enter your "Second name".')
                ->addRule(FormRules::GLOBALCHARS, 'In your "Second name" are used invalid characters.');

        $form->addText('address', 'Address', $lenght, 200)
                ->setRequired('Enter your "Address".')
                ->addRule(FormRules::GLOBALCHARS, 'In your "Address name" are used invalid characters.');

        $form->addText('city', 'City', $lenght, 100)
                ->setRequired('Enter your "City".')
                ->addRule(FormRules::GLOBALCHARS, 'In your "City" are used invalid characters.');

        $form->addText('postcode', 'Postcode', $lenght, 100)
                ->setRequired('Enter your "ZIP".')
                ->addRule(FormRules::GLOBALCHARS, 'In your "Postcode" are used invalid characters.');

        $form->addText('phone', 'Phone', $lenght, 100)
                ->setRequired('Enter your "Phone".')
                ->addRule(FormRules::PHONE, 'In your "Phone number" are used invalid characters.');

        $form->addText('email', 'E-mail', $lenght, 50)
                ->setRequired('Enter your "E-mail".')
                ->addRule(Form::EMAIL, 'Your "E-mail" is not in the correct form.');

        $form->addSelect('country', 'Country', array('Czech Republic' => 'Czech Republic', 'Netherlands' => 'Netherlands'))
                ->setPrompt('Choose a country ...')
                ->setRequired('Choose a country.');

        $form->addCheckbox('secondAddress', 'Shipping and billing address are the same');


        $form->addText('company2', 'Company', $lenght, 100)
                ->addCondition(Form::FILLED)
                ->addRule(FormRules::GLOBALCHARS, 'In your "Company name" are used invalid characters.');

        $form->addText('firstname2', 'First name', $lenght, 100)
                ->addCondition(Form::FILLED)
                ->addRule(FormRules::GLOBALCHARS, 'In your "First name" are used invalid characters.');

        $form->addText('surname2', 'Second name', $lenght, 100)
                ->addCondition(Form::FILLED)
                ->addRule(FormRules::GLOBALCHARS, 'In your "Second name" are used invalid characters.');

        $form->addText('address2', 'Address', $lenght, 200)
                ->addCondition(Form::FILLED)
                ->addRule(FormRules::GLOBALCHARS, 'In your "Address name" are used invalid characters.');

        $form->addText('city2', 'City', $lenght, 100)
                ->addCondition(Form::FILLED)
                ->addRule(FormRules::GLOBALCHARS, 'In your "City" are used invalid characters.');

        $form->addText('postcode2', 'Postcode', $lenght, 100)
                ->addCondition(Form::FILLED)
                ->addRule(FormRules::GLOBALCHARS, 'In your "Postcode" are used invalid characters.');

        $form->addText('phone2', 'Phone', $lenght, 100)
                ->addCondition(Form::FILLED)
                ->addRule(FormRules::PHONE, 'In your "Phone number" are used invalid characters.');


        $form->addSelect('country2', 'Country', array('Czech Republic' => 'Czech Republic', 'Netherlands' => 'Netherlands'))
                ->setRequired('Choose a country.');

        $form->addText('email2', 'E-mail', $lenght, 50)
                ->addCondition(Form::FILLED)
                ->addRule(Form::EMAIL, 'Your "E-mail" is not in the correct form.');

        $form->addSubmit('send', 'Submit the address form');

        $form->onSuccess[] = $this->registrationFormSubmitted;

        return $form;
    }

    public function registrationFormSubmitted(Form $form) {
        $this->database->saveAddress($form->values, $this->cart->getCart_id());

        $secur = $this->security->create()->make();
        $this->redirect('summary', $secur['timestamp'], $secur['token']);
    }
    
    public function renderSummary($timestamp, $token) {
        try {
            $this->security->create()->setTimestamp($timestamp)->setToken($token)->verifi();
        } catch (\ErrorException $e) {
            //could be sended a massage
            $this->redirect('default');
        }
        $this->template->cart = $this->cart;
        $this->template->addresses = $this->database->getCartAddresses($this->cart->getCart_id());
        
    }
    
     protected function createComponentDiscountForm() {
        $form = new Form;
        $lenght = 60;

        $form->addText('discount', 'Enter discount coupon', $lenght, 100)
                ->setRequired('Enter your discount coupon.')
                ->addRule(FormRules::GLOBALCHARS, 'In your "Discount coupon" are used invalid characters.');

        $form->addSubmit('send', 'Submit discount coupon');

        $form->onSuccess[] = $this->discountFormSubmitted;

        return $form;
    }

    public function discountFormSubmitted(Form $form) {
        $this->database->useDiscount($form->values, $this->cart->getCart_id());

        $secur = $this->security->create()->make();
        $this->redirect('summary', $secur['timestamp'], $secur['token']);
    }
    
    public function renderConfirm($timestamp, $token) {
        try {
            $this->security->create()->setTimestamp($timestamp)->setToken($token)->verifi();
        } catch (\ErrorException $e) {
            //could be sended a massage
            $this->redirect('default');
        }
        $this->template->order = $this->cart->confirm();
    }

}
