<?php

namespace App\Model\ShoppingCart;

use Nette;
use Tester;
use Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';

class ItemTest extends Tester\TestCase {
    
    /**
     * @var \Nette\DI\Container
     */
    private $container;

    /**
     * @var \App\Model\ShoppingCart\Database
     */
    private $database;
        

    function __construct(Nette\DI\Container $container) {
        $this->container = $container;
        $this->database = $this->container->getByType('\App\Model\ShoppingCart\Database');
    }

    function setUp() {
        
    }

    public function testExtension() {
        $itemattributecombination = $this->database->findItemattributecombinationById(1);
        $item = new Item($itemattributecombination);
        Assert::true($item instanceof Item);
        Assert::type('int', $item->getId());
        Assert::equal(NULL, $item->getQuantity());
        $item->insertQuantity(1);
        Assert::equal(1, $item->getQuantity());
        $item->addQuantity(1);
        Assert::equal(2, $item->getQuantity());
    }
    



}

$test = new ItemTest($container);
$test->run();
