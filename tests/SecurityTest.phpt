<?php

namespace App\Model\ShoppingCart;

use Nette;
use Tester;
use Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';

class SecurityTest extends Tester\TestCase {
    
    const KEY = '72c98b53818e6a168d645b6e6522422b';
    
    /**
     * @var \Nette\DI\Container
     */
    private $container;

    /**
     * @var \App\Model\ShoppingCart\ISecurityFactory
     */
    private $security;
        

    function __construct(Nette\DI\Container $container) {
        $this->container = $container;
        $this->security = $this->container->getByType('\App\Model\ShoppingCart\ISecurityFactory');
    }

    function setUp() {
        
    }

    public function testExtension() {
        $security = $this->security->create();
        Assert::true($security instanceof Security);
        $secur = $security->setKey(self::KEY)->make();
        Assert::count(2, $secur);
        Assert::type('string', $secur['token']);
        Assert::type('int', $secur['timestamp']);
        
        
        $security2 = $this->security->create();
        $secur2 = $security2->setKey(self::KEY)->setTimestamp($secur['timestamp'])->setToken($secur['token'])->verifi();
        Assert::true($secur2);   
    }
    



}

$test = new SecurityTest($container);
$test->run();
