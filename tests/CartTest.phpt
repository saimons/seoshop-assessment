<?php

namespace App\Model\ShoppingCart;

use Nette;
use Tester;
use Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';

class CartTest extends Tester\TestCase {

    const KEY = '72c98b53818e6a168d645b6e6522422b';
    
    /**
     * @var \Nette\DI\Container
     */
    private $container;

    /**
     * @var \App\Model\ShoppingCart\Cart
     */
    private $cart;
        

    function __construct(Nette\DI\Container $container) {
        $this->container = $container;
        $this->cart = $this->container->getByType('\App\Model\ShoppingCart\Cart');
    }

    function setUp() {
        
    }

    public function testExtension() {
        Assert::true($this->cart instanceof Cart);
        Assert::type('int', $this->cart->getCart_id());
    }

    public function testAdd() {
        $this->cart->addItem(self::KEY, 1);       
        $items = $this->cart->getItems();
        
        Assert::count(1, $items);
        
        $item = $items[self::KEY];
        Assert::true($item instanceof Item);
        
        Assert::equal(1, $this->cart->getTotalQuantity());
    }
    
    public function testUpdate() {      
        $this->cart->updateItem(self::KEY, 2);
        Assert::equal(3, $this->cart->getTotalQuantity());
    }
    
    public function testDeleteItem() {
        $this->cart->deleteItem(self::KEY);
        $items = $this->cart->getItems();
        Assert::equal(NULL, $items);
    }

}

$test = new CartTest($container);
$test->run();
