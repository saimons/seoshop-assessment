<?php

namespace App\Model\ShoppingCart;

use Nette;
use Tester;
use Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';

class StockTest extends Tester\TestCase {
    
    /**
     * @var \Nette\DI\Container
     */
    private $container;

    /**
     * @var \App\Model\ShoppingCart\Stock
     */
    private $stock;
        

    function __construct(Nette\DI\Container $container) {
        $this->container = $container;
        $this->stock = $this->container->getByType('\App\Model\ShoppingCart\Stock');
    }

    function setUp() {
        
    }

    public function testExtension() {
        Assert::true($this->stock instanceof Stock);
        $items = $this->stock->getItems();
        Assert::type('array', $items);
        foreach ($items AS $item) {
            Assert::true($item instanceof Item);
        }
    }
    



}

$test = new StockTest($container);
$test->run();
